<?php
/*
 -------------------------------------------------------------------------
 slalocale plugin for GLPI
 Copyright (C) 2014 by the slalocale Development Team.
 -------------------------------------------------------------------------

 LICENSE

 This file is part of slalocale.

 slalocale is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 slalocale is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with slalocale. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------  */

// Init the hooks of the plugins -Needed
function plugin_init_slalocale() {
   global $PLUGIN_HOOKS;

   $PLUGIN_HOOKS['csrf_compliant']['slalocale'] = true;
   
   $plugin = new Plugin();
   if ($plugin->isActivated("slalocale")) {

   }
}

// Get the name and the version of the plugin - Needed
/**
 * @return array
 */
function plugin_version_slalocale() {

   return array('name'           => __('Locales', 'slalocale'),
                'version'        => '1.0.1',
                'author'         => "<a href='http://infotel.com/services/expertise-technique/glpi/'>Infotel</a>",
                'license'        => 'GPLv2+',
                'minGlpiVersion' => '0.85'); // For compatibility / no install in version < 0.85
}

// Optional : check prerequisites before install : may print errors or add to message after redirect
function plugin_slalocale_check_prerequisites() {

   if (version_compare(GLPI_VERSION,'0.85','lt') || version_compare(GLPI_VERSION,'9.2','ge')) {
      _e('This plugin requires GLPI 0.85 or higher', 'slalocale');
      return false;
   }
   return true;
}

// Uninstall process for plugin : need to return true if succeeded
//may display messages or add to message after redirect
function plugin_slalocale_check_config() {
   return true;
}



?>
