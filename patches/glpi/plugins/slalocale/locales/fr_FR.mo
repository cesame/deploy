��          �      \      �     �     �     �  E     E   [     �     �     �     �  #     "   2     U     ]     f     j  I   �  J   �  8     (   U  b  ~     �     �       h   8  i   �        .   ,  *   [     �  *   �  &   �     �                 `   $  `   �  >   �  ,   %	                       
                   	                                                         Add a new item Automatic actions of OLA Automatic reminders of OLAs Escalations defined in the OLA will be triggered under this new date. Escalations defined in the SLA will be triggered under this new date. Internal time to own Internal time to own + Progress Internal time to own exceedeed Internal time to resolve Internal time to resolve + Progress Internal time to resolve exceedeed Locales SLA SLAs SLM Service level Service levels The assignment of a SLA to a ticket causes the recalculation of the date. The assignment of an OLA to a ticket causes the recalculation of the date. The internal time is recalculated when assigning the OLA This plugin requires GLPI 0.85 or higher Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-01-02 12:07+0100
PO-Revision-Date: 2018-01-02 12:31+0100
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.3
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter un nouvel élément Actions automatiques des OLA Rappels automatiques des OLAs Les escalades définies dans l'OLA seront également déclenchées conformément à cette nouvelle date. Les escalades définies dans la SLA seront également déclenchées conformément à cette nouvelle date. Temps de prise en charge interne Temps de prise en charge interne + Progression Temps de prise en charge interne dépassé Temps de résolution interne Temps de résolution interne + Progression Temps de résolution interne dépassé Traduction pour les OLAs SLA SLAs SLM SLM SLMs L'affectation d'une SLA à un ticket a posteriori entraîne le recalcul de la date d'échéance. L'affectation d'une OLA à un ticket a posteriori entraîne le recalcul de la date d'échéance. Le temps interne est recalculé lors de l'affectation de l'OLA Ce plugin nécessite GLPI 0.85 ou supérieur 