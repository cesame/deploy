#!/usr/bin/env python


import getopt
import sys
from ldif import LDIFParser,LDIFWriter


class MyLDIF(LDIFParser):
   def __init__(self,input,output):
      LDIFParser.__init__(self,input)
      # output_file, base64_attrs=None, cols=76, line_sep='n'
      self.base64_attrib = {
        'uid',
        'dn',
        'ENTAuxPersonMailAAF',
        'ENTPersonMailPerso',
        'ENTPersonMail',
        'ENTPersonJointure',
        'snNormalise',
        'givenNameNormalise',
        'ENTPersonStructRattach',
        'sn',
        'cn'
      }
      self.writer = LDIFWriter(output, self.base64_attrib, 200)
      self.nbEntries = 0
   def handle(self,dn,entry):
      self.nbEntries+=1
      dn2 = 'uid=%s,ou=personnes,ou=CRIF,dc=ent,dc=fr' % entry['uid'][0]
      entry2 = {
          'uid' : [entry['uid'][0]],
          'ENTAuxPersonMailAAF' : [entry['ENTAuxPersonMailAAF'][0] if 'ENTAuxPersonMailAAF' in entry else 'None'],
          'ENTPersonMailPerso' : [entry['ENTPersonMailPerso'][0] if 'ENTPersonMailPerso' in entry else 'None'],
          'ENTPersonMail' : [entry['ENTPersonMail'][0] if 'ENTPersonMail' in entry else 'None'],
          'telephoneNumber' : [entry['telephoneNumber'][0] if 'telephoneNumber' in entry else 'None'],
          'homePhone' : [entry['homePhone'][0] if 'homePhone' in entry else 'None'],
          'ENTPersonJointure' : [entry['ENTPersonJointure'][0]],
          'snNormalise' : [entry['snNormalise'][0]],
          'ENTPersonLogin' : ['None'],
          'givenNameNormalise' : [entry['givenNameNormalise'][0]],
          'ENTPersonStructRattach' : [entry['ENTPersonStructRattach'][0]],
          'personalTitle' : [entry['personalTitle'][0]],
          'cn' : [entry['cn'][0]],
          'sn' : [entry['snNormalise'][0]],
          'objectClass': ['ENTPerson', 'inetOrgPerson', 'person', 'ENTAuxPersonAccount']
      }
      self.writer.unparse(dn2,entry2)

def main(argv):
   inputfile = ''
   outputfile = ''
   try:
      opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
      print 'test.py -i <inputfile> -o <outputfile>'
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print 'test.py -i <inputfile> -o <outputfile>'
         sys.exit()
      elif opt in ("-i", "--ifile"):
         inputfile = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
   return inputfile, outputfile

inputfile, outputfile = main(sys.argv[1:])

parser = MyLDIF(open(inputfile, 'rb'), open(outputfile, 'wb'))
parser.parse_entry_records()
