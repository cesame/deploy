#! /bin/bash

STATE=0
ATTRIBUTES=''
STRUCTURE_FILE="Structures.ldif"

while getopts "hi:" OPTION
do
  case $OPTION in
    i)
      STRUCTURE_FILE=$OPTARG
      ;;
    h)
      echo "Usage: -i [path to structure.ldif]"
      exit 1
      ;;
  esac
done

if [ -e $STRUCTURE_FILE ]
then
  echo "# structure.ldif seems to be their, let's fight !"
else
  echo "# hmm, that's a bad hat Harry !! Give me a file."
  exit 1
fi

while read line
do
  if [ $STATE -eq 1 ]; then
    CHECK=`echo $line | grep dn: | wc -l`
    if [ $CHECK -ne 0 ]; then
      DN_BASE64=`echo ou=$_OU,ou=structures,ou=CRIF,dc=ent,dc=fr | base64 -w 0`
      OU_BASE64=`echo $_OU | base64 -w 0`
      NAME_BASE64=`echo $_NAME | base64 -w 0`
      echo "dn:: $DN_BASE64"
      echo -e "objectClass: ENTEtablissement"
      echo -e "objectClass: ENTStructure"
      echo -e "objectClass: organizationalUnit"
      echo -e "objectClass: top"
      echo -e "ENTStructureUAI: $_UAI"
      echo -e "ENTStructureNomCourant:: $NAME_BASE64"
      echo -e "ENTStructureJointure:: $NAME_BASE64"
      echo -e "ENTStructureSIREN: None"
      echo -e "ENTEtablissementContrat: public"
      echo -e "ENTEtablissementMinistereTutelle: EN"
      echo -e "ou:: $OU_BASE64"
      echo ""
      STATE=0
      ATTRIBUTES=''
    fi
    CHECK=`echo $line | grep -E "ENTStructureUAI:|ENTStructureNomCourant:|ou:" | wc -l`
    if [ $CHECK -ne 0 ]; then
      # ===================== Retrieve OU
      CHECK=`echo $line | grep ou: | wc -l`
      if [ $CHECK -ne 0 ]; then
        EXTRACT_UID=`echo $line | grep ou:`
        _OU=${EXTRACT_UID:4}
      fi
      # ===================== Retrieve ENTStructureNomCourant
      CHECK=`echo $line | grep ENTStructureNomCourant: | wc -l`
      if [ $CHECK -ne 0 ]; then
        EXTRACT_NAME=`echo $line | grep ENTStructureNomCourant:`
        _NAME=${EXTRACT_NAME:24}
      fi
      # ===================== Retrieve ENTStructureNomCourant
      CHECK=`echo $line | grep ENTStructureUAI: | wc -l`
      if [ $CHECK -ne 0 ]; then
        EXTRACT_UAI=`echo $line | grep ENTStructureUAI:`
        _UAI=${EXTRACT_UAI:18}
      fi
      #echo -e "$line"
    fi
  fi
  if [ $STATE -eq 0 ]; then
    CHECK=`echo $line | grep -E "^dn:.+,ou=CRIF,dc=ent,dc=fr$" | wc -l`
    if [ $CHECK -ne 0 ]; then
      #echo -e "\n$line"
      STATE=1
      continue
    fi
  fi
done < $STRUCTURE_FILE
