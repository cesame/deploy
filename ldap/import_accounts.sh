#!/bin/bash
# ========================================
#
# Script used by SCC to import accounts
#
# ========================================
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
prg="inject_ldif"
tmp_dir="${HOME}/data"
db_path="/var/lib"
exclude_pattern="^search:|^result:"
current_base ()
{
  basename `ldapsearch -LLL -Y EXTERNAL -H ldapi:/// \
    -b 'olcDatabase={1}hdb,cn=config' \
    olcDbDirectory 2>/dev/null | \
    grep olcDbDirectory | cut -d' ' -f 2`
}
switch_base ()
{
cat <<EOF | ldapmodify -Y EXTERNAL -H ldapi:/// 2>/dev/null >/dev/null
dn: olcDatabase={1}hdb,cn=config
changetype: modify
replace: olcDbDirectory
olcDbDirectory: ${db_path}/${1}
EOF

  if [ "$?" != "0" ]; then
    logger -t ${prg} "Error: can't switch to base ${altdb}"
    exit 1
  fi
}
abort_injection ()
{
  logger -t ${prg} "Abord injection: ${_err}"
  exit 1
}
inject_parent_dn ()
{
cat <<'EOF' | slapadd ${slapadd_arg}
dn: dc=ent,dc=fr
objectclass: dcObject
objectclass: organization
o: ENT
dc: ent
dn: ou=CRIF,dc=ent,dc=fr
objectClass: organizationalUnit
objectClass: top
dn: ou=personnes,ou=CRIF,dc=ent,dc=fr
objectClass: organizationalUnit
objectClass: top
dn: ou=structures,ou=CRIF,dc=ent,dc=fr
objectClass: organizationalUnit
objectClass: top
EOF

}
logger -t ${prg} "START"
if [ ! -f ${tmp_dir}/NEED_UPGRADE ]; then
  logger -t ${prg} "Upgrade not required"
  exit 0
fi
# Find the Last LDIF filenames
personnes_ldif=`head -1 ${tmp_dir}/current_personnes 2>&1`
if [ "$?" != "0" ]; then
  logger -t ${prg} "Error: ${personnes_ldif}"
  exit 1
fi
structures_ldif=`head -1 ${tmp_dir}/current_structures 2>&1`
if [ "$?" != "0" ]; then
  logger -t ${prg} "Error: ${structures_ldif}"
  exit 1
fi
# Check LDIF existence
if [ ! -f ${tmp_dir}/${personnes_ldif} ]; then
  logger -t ${prg} "LDIF ${tmp_dir}/${personnes_ldif} is missing"
elif [ ! -f ${tmp_dir}/${structures_ldif} ]; then
  logger -t ${prg} "LDIF ${tmp_dir}/${structures_ldif} is missing"
fi
# Cleanup LDIF : exclude dump exit code and duplicates
grep -v -E "^[^:]*:\s$|^search:|^result:" ${tmp_dir}/${personnes_ldif} \
  | sed -e 's/<//g' -e 's/>//g' \
  | awk '/^dn: /{p=!($0 in a);a[$0]}p' >${tmp_dir}/Personnes.ldif
logger -t ${prg} "cleanup ${tmp_dir}/${personnes_ldif}"\
  "in ${tmp_dir}/Personnes.ldif"
grep -v -E "^[^:]*:\s$|^search:|^result:" ${tmp_dir}/${structures_ldif} \
  | sed -e 's/<//g' -e 's/>//g' \
  | awk '/^dn: /{p=!($0 in a);a[$0]}p' >${tmp_dir}/Structures.ldif
logger -t ${prg} "cleanup ${tmp_dir}/${structures_ldif}" \
  "in ${tmp_dir}/Structures.ldif"
# Determine alternate Base
b=`current_base`
if [ "${b}" = "ldap1" ]; then
  altdb="ldap2"
  slapconf="/etc/openldap/slapd2.conf"
  slapadd_arg="-f ${slapconf} -o schema-check=no"
elif [ "${b}" = "ldap2" ]; then
  altdb="ldap1"
  slapconf="/etc/openldap/slapd1.conf"
  slapadd_arg="-f ${slapconf} -o schema-check=no"
else
  logger -t ${prg} "Error: Can't determine alternate LDAP Base."\
    "Current LDAP base is: ${b}"
  exit 1
fi
altdb_dir="${db_path}/${altdb}"
# Prepare temporary DB
rm -rf ${altdb_dir}
mkdir -p ${altdb_dir}
_err=`chmod 700 ${altdb_dir} 2>&1`
if [ "$?" != "0" ]; then
  abort_injection
fi
logger -t ${prg} "Inject parent DN into temporary DB"
_err=`inject_parent_dn 2>&1`
if [ "$?" != "0" ]; then
  abort_injection
fi
logger -t ${prg} "Inject Personnes LDIF into temporary DB"
_err=`slapadd ${slapadd_arg} -l ${tmp_dir}/Personnes.ldif 2>&1`
if [ "$?" != "0" ]; then
  abort_injection
fi
logger -t ${prg} "Inject Structures LDIF into temporary DB"
_err=`slapadd ${slapadd_arg} -l ${tmp_dir}/Structures.ldif 2>&1`
if [ "$?" != "0" ]; then
  abort_injection
fi
_err=`chown -R ldap: ${altdb_dir} 2>&1`
if [ "$?" != "0" ]; then
  abort_injection
fi
_err=`chcon -Rv -u system_u -r object_r -t slapd_db_t ${altdb_dir} 2>&1`
if [ "$?" != "0" ]; then
  abort_injection
fi
switch_base ${altdb}
logger -t ${prg} "switch to ${altdb}"
rm -f ${tmp_dir}/NEED_UPGRADE
logger -t ${prg} "END"
