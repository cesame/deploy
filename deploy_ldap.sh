#!/bin/bash
exec 2>&1

# ==================================================
#
# Check a package and install it if not present
# @param $1 package name
# ==================================================
Install(){
  package=$1
  installed=$(dpkg -l $package | awk '{print $1}' | grep "ii" | wc -l)
  if [ "$installed" = "0" ]; then
    sudo DEBIAN_FRONTEND=noninteractive apt-get install $package
  else
    echo "$package already installed -> do nothing"
  fi
}

#set -x
# ==================================================
#
#	Arguments - change the passwords !
#
# ==================================================

# define database location
BDD_DIR="/var/lib/ldap/bdd_cridf"
# define password for cn=admin,dc=crdp,dc=local
PASSWORD="change_it"
# define password for cn=admin,cn=config
ROOT_PASSWORD="change_it"
# define domain components dc=dc1,dc=dc2
DC1="ent"
DC2="fr"

while getopts "hd:p:r:c:" OPTION
do
    case $OPTION in
        d)
            BDD_DIR=$OPTARG
            ;;
        p)
            PASSWORD=$OPTARG
            ;;
        r)
            ROOT_PASSWORD=$OPTARG
            ;;
        c)
            DC1=$(echo $OPTARG | cut -d. -f1)
            DC2=$(echo $OPTARG | cut -d. -f2)
            ;;
        h)
            echo "Usage: -d [bdd directory] -p [admin password] -r [cn=config admin password] -c [domain.local]"
            exit 1
            ;;
    esac
done

# ==============================================
#
#	Prepare installation
#
# ==============================================

SUFFIX="dc=$DC1,dc=$DC2"
# Install openldap server and openldap tools
# Install aptitude
Install slapd
Install ldap-utils
Install ntp

# Remove default debian database
CONF_FILE="/etc/ldap/slapd.d/cn=config/olcDatabase={1}hdb.ldif"
if [ -e $CONF_FILE ]
then
	echo "==========> Remove default debian database"
	/etc/init.d/slapd stop
	rm $CONF_FILE
	/etc/init.d/slapd start
else
	echo "==========> Default database already removed"
fi

# Create directory to store database
if [ -d $BDD_DIR ]
then
echo "==========> $BDD_DIR already exists. Removing the current database"
rm -rf $BDD_DIR
fi
echo "==========> create $BDD_DIR directory"
mkdir $BDD_DIR
chown openldap:openldap $BDD_DIR

/etc/init.d/slapd stop
CONF_FILE_MDB="/etc/ldap/slapd.d/cn=config/olcDatabase={1}mdb.ldif"
OVERLAYS_MDB="/etc/ldap/slapd.d/cn=config/olcDatabase={1}mdb"
if [ -e $CONF_FILE_MDB ]
then
echo "==========> {1}mdb conf file already exists"
rm $CONF_FILE_MDB
echo "==========> {1}mdb conf file deleted"
fi
if [ -d $OVERLAYS_MDB ]
then
echo "==========> {1}mdb overlays exists"
rm -R $OVERLAYS_MDB
echo "==========> {1}mdb overlays deleted"
fi

CONF_FILE_MDB="/etc/ldap/slapd.d/cn=config/olcDatabase={2}mdb.ldif"
OVERLAYS_MDB="/etc/ldap/slapd.d/cn=config/olcDatabase={2}mdb"
if [ -e $CONF_FILE_MDB ]
then
echo "==========> {2}mdb conf file already exists"
rm $CONF_FILE_MDB
echo "==========> {2}mdb conf file deleted"
fi
if [ -d $OVERLAYS_MDB ]
then
echo "==========> {2}mdb overlays exists"
rm -R $OVERLAYS_MDB
echo "==========> {2}mdb overlays deleted"
fi
/etc/init.d/slapd start

# ==========================================
#
#	Create RootPW
#	for cn=config database
# for Ubuntu, olcRootDN does not exists
# at first time
# ==========================================
ROOT_PASSWORD_ENCRYPTED=$(slappasswd -s $ROOT_PASSWORD)

CHECK=`ldapsearch -Y EXTERNAL -H ldapi:/// -b cn=config olcDatabase=config | awk '{if ($1=="olcRootDN:") print $2}' | wc -l`
if [ $CHECK -ne 0 ];
then
  echo "==========> Replace RootPW RootDN to cn=config database"

cat > "/root/add_rootpw.ldif" <<EOF
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: cn=admin,cn=config
-
replace: olcRootPW
olcRootPW: $ROOT_PASSWORD_ENCRYPTED
EOF

else
  echo "==========> Add RootPW RootDN to cn=config database"

cat > "/root/add_rootpw.ldif" <<EOF
dn: olcDatabase={0}config,cn=config
changetype: modify
add: olcRootDN
olcRootDN: cn=admin,cn=config
-
add: olcRootPW
olcRootPW: $ROOT_PASSWORD_ENCRYPTED
EOF

fi
# Add RootPW
ldapmodify -Y EXTERNAL -H ldapi:/// -f /root/add_rootpw.ldif
rm /root/add_rootpw.ldif

# ==========================================
#
#       Modify ACL to cn=config database
#       and frontend database
#	Thus, TCP connections can be performed
#
# ==========================================
echo "==========> Delete olcAccess for cn=config and frontend databases"
cat > "/root/delete_acl.ldif" <<EOF
dn: olcDatabase={-1}frontend,cn=config
changetype: modify
delete: olcAccess

dn: olcDatabase={0}config,cn=config
changetype: modify
delete: olcAccess

EOF

ldapmodify -w $ROOT_PASSWORD -D "cn=admin,cn=config" -f /root/delete_acl.ldif
rm /root/delete_acl.ldif



echo "==========> add olcAccess for cn=config and frontend databases"
cat > "/root/add_acl.ldif" <<EOF
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcAccess
olcAccess: to * by dn="cn=admin,cn=config" manage by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth write by * break
olcAccess: to dn="cn=subschema" by * read
olcAccess: to dn="" by * read

dn: olcDatabase={-1}frontend,cn=config
changetype: modify
replace: olcAccess
olcAccess: to * by dn="cn=admin,cn=config" manage by dn.exact="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" write by * break
olcAccess: to dn="cn=subschema" by * read
olcAccess:  to dn="" by * read

EOF

ldapmodify -w $ROOT_PASSWORD -D "cn=admin,cn=config" -f /root/add_acl.ldif
rm /root/add_acl.ldif



# ==========================================
#
#	Create MDB database
#
# ==========================================

# Encrypt password
PASSWORD_ENCRYPTED=$(slappasswd -s $PASSWORD)

BACK_MDB_LOADED=$(ldapsearch -D "cn=admin,cn=config" -b "cn=config" -w $ROOT_PASSWORD "objectClass=olcModuleList" | grep back_mdb | wc -l)
if [ $BACK_MDB_LOADED -eq 0 ];
then
echo "==========> Add back_mdb module"
cat > "/root/add_mdb_module.ldif" <<EOF
# Activate module used to create mdb database
dn: cn=module,cn=config
objectClass:olcModuleList
cn:module
olcModulepath: /usr/lib/ldap
olcModuleload: back_mdb.la
EOF

ldapadd -D "cn=admin,cn=config" -w $ROOT_PASSWORD -f /root/add_mdb_module.ldif
rm /root/add_mdb_module.ldif
fi

echo "==========> create mdb database"
cat > "/root/make_bdd.ldif" <<EOF
# Database parameters
dn: olcDatabase={1}mdb,cn=config
objectClass: olcDatabaseConfig
objectClass: olcMdbConfig
olcDatabase: {1}mdb
olcDbDirectory: $BDD_DIR/
olcSuffix: $SUFFIX
olcRootDN: cn=admin,$SUFFIX
olcRootPW: $PASSWORD_ENCRYPTED
olcSizeLimit: 500
olcDbMaxsize: 104857600
olcDbIndex: objectClass eq
olcLastMod: TRUE
olcAccess: to attrs=userPassword by dn="cn=admin,$SUFFIX" write by anonymous auth by self write by * none
olcAccess: to attrs=shadowLastChange by self write by anonymous none
olcAccess: to dn.base="" by anonymous none
olcAccess: to * by dn="cn=admin,$SUFFIX" write by users read by anonymous none
EOF

ldapadd -D "cn=admin,cn=config" -w $ROOT_PASSWORD -f /root/make_bdd.ldif
rm /root/make_bdd.ldif


# ==========================================
#
#	Create RootDSE
#
# ==========================================
echo "==========> create RootDSE"
cat > "/root/make_rootdse.ldif" <<EOF
dn: $SUFFIX
objectClass: dcObject
objectClass: organization
o: $DC1.$DC2
dc: $DC1
description: Racine du ldap
EOF

ldapadd -w $PASSWORD -D "cn=admin,$SUFFIX" -f /root/make_rootdse.ldif
rm /root/make_rootdse.ldif

# ==========================================
#
#	Add SDET and ENT schemas
#
# ==========================================
echo "==========> add SDET and ENT schemas"
SDET_FILE="/etc/ldap/slapd.d/cn=config/cn=schema/cn={4}sdet.ldif"
if [ -e $SDET_FILE ]
then
  echo "SDET schema already installed"
else
  ldapadd -D "cn=admin,cn=config" -w $ROOT_PASSWORD -f schemas/sdet.ldif
fi

ENT_FILE="/etc/ldap/slapd.d/cn=config/cn=schema/cn={5}ent.ldif"
if [ -e $ENT_FILE ]
then
  echo "ENT schema already installed"
else
  ldapadd -D "cn=admin,cn=config" -w $ROOT_PASSWORD -f schemas/ent.ldif
fi

# ==========================================
#
#	Init structure
#
# ==========================================
echo "==========> Init DIT structure"
TEST=$(ldapsearch -D "cn=admin,dc=ent,dc=fr" -b "dc=ent,dc=fr" -x -w $PASSWORD dn 2>&1)
CHECK=`echo $TEST | grep "ou=CRIF" | wc -l`
if [ $CHECK -ne 0 ];
then
  echo "ou=CRIF already created"
else

cat > "/root/make_crif_ou.ldif" <<EOF
dn: ou=CRIF,$SUFFIX
changetype: add
objectClass: organizationalUnit
objectClass: top
ou: CRIF
EOF

  ldapadd -x -w $PASSWORD -D "cn=admin,$SUFFIX" -f /root/make_crif_ou.ldif
  rm /root/make_crif_ou.ldif

fi


TEST=$(ldapsearch -D "cn=admin,dc=ent,dc=fr" -b "dc=ent,dc=fr" -x  -w $PASSWORD dn 2>&1)
CHECK=`echo $TEST | grep "ou=personnes" | wc -l`
if [ $CHECK -ne 0 ];
then
  echo "ou=personnes already created"
else

cat > "/root/make_crif_ou.ldif" <<EOF
dn: ou=personnes,ou=CRIF,$SUFFIX
changetype: add
objectClass: organizationalUnit
objectClass: top
ou: personnes
EOF

  ldapadd -x -w $PASSWORD -D "cn=admin,$SUFFIX" -f /root/make_crif_ou.ldif
  rm /root/make_crif_ou.ldif

fi

TEST=$(ldapsearch -D "cn=admin,dc=ent,dc=fr" -b "dc=ent,dc=fr" -x  -w $PASSWORD dn 2>&1)
CHECK=`echo $TEST | grep "ou=structures" | wc -l`
if [ $CHECK -ne 0 ];
then
  echo "ou=structures already created"
else

cat > "/root/make_crif_ou.ldif" <<EOF
dn: ou=structures,ou=CRIF,$SUFFIX
changetype: add
objectClass: organizationalUnit
objectClass: top
ou: structures
EOF

  ldapadd -x -w $PASSWORD -D "cn=admin,$SUFFIX" -f /root/make_crif_ou.ldif
  rm /root/make_crif_ou.ldif

fi
