# Réversibilité CESAME

## 1. Introduction
L'objet de ce document est de présenter l'architecture technique préssentie suite à la phase de réversibilité réalisée entre le prestataire SCC, opérateur Cesame dans le cadre du marché MCO # ??? et le service de la Transformation Numérique du pôle Lycées. Pour rappel, l'objectif visé par le Pôle Lycées se décline selon les phases suivantes :
- piloter et recetter la réversibilité depuis l'environnement SCC vers un environnement provisoire de qualification hébergé chez OVH (financé sur le marché # ?? dit de "RUN" de l'accord-cadre #???)
- Qualifier l'architecture cible afin de rendre l'infrastructure solide et évolutive
- Dans le cadre du marché # ??, solliciter CGI pour mettre à disposition l'infrastructure cible et assurer la supervision des serveurs déployés.
- Déployer l'infrastructure avec le soutien de SCC dans le cadre de l'opération de réversibilité
- Assurer la MCO de la solution durant le temps nécessaire à la rédaction du futur marché TMA
- Rédiger et publier le futur marché de TMA
- Assurer la réversibilité vers le nouveau prestataire de TMA

## 2. Architecture applicative

La plateforme Cesame permet aux lycées, à certains prestataires et aux agents Région de réaliser les tâches suivantes :
- Rédiger et suivre de tickets d'incidents sur les serveurs Lycées ou les postes clients dans le cadre du marché MCO
- Rédiger et suivre des demandes de MISSIONS dans le cadre du marché MCO pour lancer des études de converture Wifi ou réaliser des câblages et installations.
- Inventorier les équipements installés dans les lycées à l'aide d'une application cliente installée sur les postes de travail.

CESAME est, à l'heure où ce document est écrit, appuyé sur la solution libre GLPI 9.1.7.1. Le schéma de synthèse ci-dessous présente l'ensemble des applicatifs nécessaires au bon fonctionnement d'une instance de cette solution.

![Architecture Cesame](images/cesame_architecture.png "Vue de l'architecture CESAME")

Il est à noter que Cesame devra disposer a minima de 3 instances :
- 1 instance de production
- 2 instances isofonctionnelles à la production dites de préproduction et/ou de développement

Ce qui différencie les 3 environnements est visible au niveau des espaces de stockage (en bleu et en mauve sur le schéma). L'ensemble des services nécessaires doit être strictement identique entre les environnements sauf dans le cas d'une évolution. Toute évolution apportée DEVRA être tracée et versionnée.

## 3. Architecture infrastructure

### 3.1 Schéma d'Architecture global

### 3.2 Équilibrage de charge

### 3.3 Deni de service


## 4. Déploiements

### 4.1 Déploiement de GLPI
- Script idempotent de déploiement en DEV et PROD

### Déploiement LDAP
- Script idempotent de déploiement d'un openldap

## 5. Annuaires et Bases de données

GLPI utilise un annuaire LDAP pour réaliser les recherches sur les utilisateurs et les groupes mais également au moment de l'authentification.

### 5.1 Synchronisation LDAP
- Chaque nuit, un **CRON** sur l'instance ldap_builder va solliciter l'API de MonLycee.net pour extraire les comptes avec les profils ENT.... Il va alors forger 2 fichiers (personnes.ldif et structures.ldif) qu'il va importer dans un ldap local.
- Chaque instance GLPI, toujours par l'intermédiaire d'un CRON va récupérer la BDD ainsi générée et remplacer la BDD LDAP locale par ces nouveaux fichiers.

### 5.2 Synchronisation des Bases
A partir du serveur de backup, il est possible de déployer une sauvegarde donnée (BDD+Files) sur les instances de preprod et de dev


## 6. Réversibilité : D-DAY

Voici l'ensemble des opérations à réaliser pour que la bascule se fasse sans encombre.

### Lundi 27 août
- [STNLP] : Publipostage pour annoncer la fermeture de Cesame
- [STNLP] : ticket au 19 pour redirection du domaine cesame.iledefrance.fr pour le lundi 3 septembre
- [STNLP] : Préparation des environnements PROD/PREPROD/DEV (Installation GLPI, LDAP, CERTIFICAT SERVEUR, CRONS LDAP, CRONS BACKUPS BDD, PAGE DE MAINTENANCE)

### Vendredi 31 août
- [SCC] : Fermeture de cesame.iledefrance.fr à 15h
- [SCC] : Un backup FILES et dump BDD doivent être déposés sur le FTP SCC après la fermeture.

### Lundi 3 septembre
- [STNLP] : NGINX redirige cesame.iledefrance.fr et cesame-dev.iledefrance.fr vers la page de maintenance locale. cesame-preprod.iledefrance.fr vers l'instance de production
- [DSI] : Le nom de domaine doit être redirigé dans la journée vers l'IP 54.36.51.73 par le 19 (Attention : Il faut modifier l'entrée du serveur DNS Région mais également le PROXY qui semble nécessiter un paramétrage spécifique)
- [STNLP] : Importation de la BDD et des FILES sur la pre-prod
- [STNLP] : Application des patchs de config (notamment IP LDAP)
- [STNLP] : Lancement des opérations de recettes (scénarios écrits, notamment CAS, Recherche LDAP)
- [STNLP] : BACKUP des FILES et de la BDD
- [STNLP] : Restauration des FILES et de la BDD sur les 2 autres instances
- [STNLP] : (17h) NGINX redirige cesame vers l'instance de pre-production
- [STNLP] : Publipostage pour annonce de la réouverture de GLPI
