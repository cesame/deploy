#!/bin/bash

#########################################
#
#  idempotent script used to prepare
#  docker environnement
#  @usage:
#    bash ./deploy_glpi.sh
#  @authors: pascal.fautrero@iledefrance.fr
#
#########################################

set -e
set -x

web_dir=/cesame # web_dir must be an absolute path !
directory=glpi
tempdir=tmp
glpi_version="9.1.7.1"
mysql_root_pwd=$(tr -dc A-Za-z0-9 < /dev/urandom | head -c 8 | xargs)
cesame_branch=master
script_dir=${0%/*}
patches_dir=$(cd "$script_dir/patches"; pwd)
themes_dir=$(cd "$script_dir/themes"; pwd)
pics_dir=$(cd "$script_dir/pics"; pwd)
hostname=cesame-dev
domain=iledefrance.fr
# Define target environment PROD - DEV

environment="PROD"
apache_user=www-data
apache_group=www-data

#########################################
#
# used to install/reinstall mysql-server
# Warning : mysql root password is modified
# each time we start this function
#
# @arg1 db root password
#
#########################################

function install_mysql() {

  mysql_root_pwd=$1

  sudo apt-get install -y debconf-utils
  echo PURGE | sudo debconf-communicate mysql-server-5.7
  sudo apt-get -y remove --purge mysql-server*
  if [ $? != 0 ];
  then
    echo "can't remove mysql server properly. Maybe some container is working with it."
    exit
  fi
  # removing files is mandatory
  sudo rm -fr /var/lib/mysql

  # be careful : tabs are needed instead of spaces in strings given to debconf-set-selections
  sudo debconf-set-selections <<< "mysql-server-5.7	mysql-server/root_password	password	$mysql_root_pwd"
  sudo debconf-set-selections <<< "mysql-server-5.7	mysql-server/root_password_again	password	$mysql_root_pwd"
  sudo apt-get install -y mysql-server
  cat > ~/.my.cnf <<EOF
[client]
user=root
password=$mysql_root_pwd
EOF

}

########################################
#
#
#
########################################

function install_packages() {

  environment=$1
  web_dir=$2
  directory=$3

  sudo apt-get update
  sudo apt-get install -y curl php7.0 php7.0-json php7.0-mysql php7.0-curl php7.0-ldap php7.0-imap php7.0-gd apache2 \
                          php7.0-xmlrpc php7.0-xml php7.0-zip sudo php7.0-cli php7.0-mbstring ca-certificates ntp unzip \
                          pv cgroup-lite aufs-tools git npm nodejs-legacy

  if [ "$environment" = "DEV" ];then
    sudo apt-get install -y php7.0-intl curl
  fi
  sudo service apache2 restart

}

########################################
#
# @arg1 database name and database user
# @arg2 password db user
# @arg3 mysql root password
#
########################################

function create_db(){

  username=$1
  userpassword=$2
  mysql_root_pwd=$3

  echo "DROP DATABASE IF EXISTS $username" | mysql --user=root --password=$mysql_root_pwd
  echo "CREATE DATABASE IF NOT EXISTS $username DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;" | mysql --user=root --password=$mysql_root_pwd
  echo "GRANT SELECT, CREATE TEMPORARY TABLES, LOCK TABLES, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER ON $username.* TO '$username'@'localhost' IDENTIFIED BY '$userpassword';" | mysql --user=root --password=$mysql_root_pwd

}

########################################
#
#  get glpi source code
#
########################################

function get_glpi(){

  web_dir=$1
  directory=$2
  version=$3
  cd /tmp/glpi
  sudo wget https://github.com/glpi-project/glpi/releases/download/$version/glpi-$version.tgz
  tar -C $web_dir -zxvf glpi-$version.tgz
  cd $web_dir/$directory
  sudo chown -R $USER:$apache_group $web_dir
  chmod g+w files
  chmod g+w config

  cd plugins
  # plugin slalocale
  git clone https://gitlab.com/cesame/slalocale.git

  # plugin groupassignment
  git clone https://gitlab.com/cesame/groupassignment.git

  # plugin news
  # https://github.com/pluginsGLPI/news/archive/1.3.2.5.tar.gz
  git clone https://gitlab.com/cesame/news.git

  # plugin fields
  # https://github.com/pluginsGLPI/fields/archive/1.6.1.tar.gz
  git clone https://gitlab.com/cesame/fields.git

  # plugin timelineticket
  # https://github.com/pluginsGLPI/timelineticket/archive/0.90+1.0.tar.gz
  git clone https://gitlab.com/cesame/timelineticket.git


  # plugin behaviors
  # https://forge.glpi-project.org/attachments/download/2178/glpi-behaviors-1.3.tar.gz
  git clone https://gitlab.com/cesame/behaviors.git


  # plugin mydashboard
  # https://github.com/InfotelGLPI/mydashboard/archive/1.3.2.tar.gz
  git clone https://gitlab.com/cesame/mydashboard.git


  # plugin fusioninventory
  # https://github.com/fusioninventory/fusioninventory-for-glpi/archive/glpi9.1+1.1.tar.gz
  git clone https://gitlab.com/cesame/fusioninventory.git


  # plugin pdf
  # https://forge.glpi-project.org/attachments/download/2171/glpi-pdf-1.1.tar.gz
  git clone https://gitlab.com/cesame/pdf.git


  # plugin datainjection
  # https://github.com/pluginsGLPI/datainjection/archive/2.4.2.tar.gz
  git clone https://gitlab.com/cesame/datainjection.git

  # plugin browsernotification
  # https://github.com/edgardmessias/browsernotification/archive/1.1.8.tar.gz
  git clone https://gitlab.com/cesame/browsernotification.git


  # plugin reports
  # https://forge.glpi-project.org/attachments/download/2173/glpi-reports-1.10.tar.gz
  git clone https://gitlab.com/cesame/reports.git

  # plugin tags
  mkdir $web_dir/$directory/plugins/tag
  cd /tmp/glpi
  mkdir tag
  sudo wget https://github.com/pluginsGLPI/tag/archive/2.0.1.tar.gz
  tar -C /tmp/glpi/tag -zxvf 2.0.1.tar.gz
  cp -R /tmp/glpi/tag/tag-2.0.1/* $web_dir/$directory/plugins/tag


  # Add CESAME theme
  cd $web_dir/$directory
  cp -R $themes_dir/* css/palettes

  # Apply Core patches (InfoTel stuff)
  # cd $web_dir
  # patch -f -p0 < $patches_dir/glpi.diff
  cd $web_dir/$directory
  cp -R $patches_dir/glpi/* .


  # Add pics
  cd $web_dir/$directory
  cp -R $pics_dir/* pics/
}


########################################
#
# directories used to build glpi
#
########################################

function create_directories(){
  USER=$(whoami)
  GROUP=$USER
  if [ ! -d $web_dir ]; then
    sudo mkdir $web_dir
  fi
  if [ -d $web_dir/$directory  ];then
    sudo rm -R $web_dir/$directory
  fi
  if [ ! -d "$web_dir/$directory" ]; then
    sudo mkdir $web_dir/$directory
  fi
  if [ -d "/tmp/glpi"  ];then
    sudo rm -R /tmp/glpi
  fi
  if [ ! -d "/tmp/glpi" ]; then
    sudo mkdir /tmp/glpi
  fi


  #sudo chown -R $USER:$GROUP $web_dir
  sudo chown -R $USER:$apache_group $web_dir

# add cron task
sudo tee /etc/cron.d/glpi < /dev/null <<EOF
*/5 * * * * su -s /bin/bash $USER -c "php $web_dir/$directory/front/cron.php" >/dev/null
EOF


}

########################################
#
#  prepare virtualhost
#
########################################

VirtualHost(){
  hostname=$1
  domain=$2
  serverName=$3
  apache_user=$4
  web_dir=$5
  directory=$6

  sudo tee /etc/apache2/sites-available/$hostname.conf < /dev/null <<EOF

<VirtualHost *:80>
  ServerName $hostname.$domain
  ServerAdmin pascal.fautrero@iledefrance.fr
  DocumentRoot $web_dir/$directory
  <Directory $web_dir/$directory>
    AllowOverride AuthConfig Indexes Limit Options FileInfo
    Options Indexes FollowSymLinks MultiViews
    Require all granted
  </Directory>
  ProxyPassMatch ^/(.*\.php(/.*)?)$ fcgi://127.0.0.1:9000/$web_dir/$directory/\$1
</VirtualHost>

EOF

  sudo sed -i "s|\/run\/php\/php7.0-fpm.sock|127.0.0.1:9000|g" "/etc/php/7.0/fpm/pool.d/www.conf"

  if [ -e /etc/apache2/sites-available/000-default.conf ]; then
    sudo a2dissite 000-default
  fi
  if [ -e /etc/apache2/sites-available/default-ssl.conf ]; then
    sudo a2dissite default-ssl
  fi
  if [ -e /etc/apache2/sites-available/$hostname.conf ]; then
    sudo a2ensite $hostname
  fi

  sudo a2enmod proxy_fcgi
  sudo service apache2 restart
  sudo service php7.0-fpm restart

}




########################################
#
#  main
#
########################################


while getopts "he:f" OPTION
do
    case $OPTION in
        f)
            use_git_cache=no
            ;;
        e)
            environment=$OPTARG
            ;;
        h)
            echo "Usage: -e [PROD|DEV]  define environment. PROD will remove git folders. DEV will install behat and phpunit and launch unit tests"
            echo "       -f             do not use git local cache and force git clone for all projects"
            exit 1
            ;;
    esac
done

install_packages $environment $web_dir $directory
create_directories
get_glpi $web_dir $directory $glpi_version
if [ "$environment" = "DEV" ];then
  install_mysql $mysql_root_pwd

  # mysql stuffs - db creation + db user creation
  password=$(tr -dc A-Za-z0-9 < /dev/urandom | head -c 20 | xargs)
  create_db glpi $password $mysql_root_pwd

  # define virtualhost on dev machine
  VirtualHost $hostname $domain $hostname $apache_user $web_dir $directory

  # reminder of DB password
  echo "**********************************************************"
  echo "*"
  echo "* DB user : glpi"
  echo "* DB pwd : $password"
  echo "*"
  echo "**********************************************************"
fi
