import re
import getopt
import sys
import os
mysql=open('/var/lib/mysql-files/createdatabase.sql', 'w')
parallel_target=open('/var/lib/mysql-files/test_parallel.sh', 'w')
source_file = ''

try:                                
  opts, args = getopt.getopt(sys.argv[1:], "hs:", ["help", "source="])
except getopt.GetoptError:          
  sys.exit(2)                     
for opt, arg in opts:
  if opt in ("-h", "--help"):
    print("python make_load_data_files.py --source=/tmp/db.sql")                 
    sys.exit()                  
  elif opt in ("-s", "--source"):
    source_file = arg  

if os.path.isfile(source_file):
  with open(source_file) as f:
    for line in f:
      if 'DROP TABLE IF EXISTS' in line:
        # Create new file
        tablename = re.search('DROP TABLE IF EXISTS `(.*)`', line, re.IGNORECASE).group(1)
        csvfile=open('/tmp/%s.sql' %tablename, 'w')
        print(tablename)
        parallel_target.write('mysqlimport --use-threads=20 glpi /var/lib/mysql-files/%s.sql\n' % tablename)
        mysql.write(line)
      else:
        if 'INSERT INTO' in line:
          line_fixed = line[len('INSERT INTO `%s` VALUES (' %tablename) :-3]
          line_fixed = line_fixed.replace('),(', '\n')
          line_fixed = line_fixed.replace(',', '\t')
          csvfile.write(line_fixed)
        else:
          mysql.write(line)
else:
  print("--source is missing")
