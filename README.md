## Install GLPI for CESAME

### Prerequisites
OS : ubuntu 16.04

`sudo apt-get install git`

### Installation

Cesame installation is performed in 6 steps. First of all, you need following files :
- ~/data-cesame/Personnes.ldif
- ~/data-cesame/Structures.ldif
- ~/data-cesame/cesame.sql.gz
- ~/data-cesame/files-users.zip

#### Step1 : GLPI installation

To deploy GLPI on a DEV environnement :
```
cd ~
git clone https://gitlab.com/cesame/deploy.git
cd deploy
./deploy_glpi.sh -e DEV
```
Script displays DB password for glpi user. Now, go to cesame-dev.iledefrance.fr and configure glpi

#### Step2 : OPENLDAP installation

To deploy a local LDAP server compatible with cesame :
```
cd ~/deploy
./deploy_ldap.sh
```
For information, admin password is set to "change_it" by default. This password is used in Step5.


#### Step3 : Import DB

```
# get DB password for glpi user
glpi_db_password=$(cat /cesame/glpi/config/config_db.php | grep '$dbpassword' | awk '{print $4}' | tr -d ';' | tr -d "'")
# import DB
pv ~/data-cesame/cesame.sql.gz | gunzip | mysql -u glpi -p$glpi_db_password glpi

```

#### Step4 : Import users files

```
cd ~/data-cesame
tar -xvzf files-users.tar.gz
cp -r files/* /cesame/glpi/files/
```


#### Step5 : Import DIT

This step should be added in a cron task to be executed each night (importing users from ENT)
```
cd ~/deploy/ldap
# first step : clean up ldif files
./fix_import_persons.py -i ~/data-cesame/Personnes.ldif -o ~/data-cesame/Personnes_fixed.ldif
./fix_imports_structure.sh -i ~/data-cesame/Structures.ldif > ~/data-cesame/Structures_fixed.ldif

# second step : import data very quickly (thanks to eatmydata)
service slapd stop
apt install eatmydata python-ldap
eatmydata slapd -h ldap:/// -g openldap -u openldap -F /etc/ldap/slapd.d
ldapadd -c -w "change_it" -D "cn=admin,dc=ent,dc=fr" -f ~/data-cesame/Personnes_fixed.ldif
ldapadd -c -w "change_it" -D "cn=admin,dc=ent,dc=fr" -f ~/data-cesame/Structures_fixed.ldif
kill $(ps aux | grep openldap | awk '{print $2}')
service slapd start

```

#### Step6 : Fix GLPI config

```
# get DB password for glpi user
glpi_db_password=$(cat /cesame/glpi/config/config_db.php | grep '$dbpassword' | awk '{print $4}' | tr -d ';' | tr -d "'")
# fix user id 2 (glpi admin)
glpi_user_password=change_it
hashed_pwd=$(htpasswd -bnBC 10 "" $glpi_user_password | tr -d ':')
echo "UPDATE glpi.glpi_users SET is_active = 1 WHERE id='2';" | mysql --user=glpi --password=$glpi_db_password
echo "UPDATE glpi.glpi_users SET password = '$hashed_pwd' WHERE id='2';" | mysql --user=glpi --password=$glpi_db_password
echo "UPDATE glpi.glpi_users SET end_date = '2050-12-31 17:00:00' WHERE id='2';" | mysql --user=glpi --password=$glpi_db_password

```
You can use "glpi" user on cesame-dev.iledefrance.fr/?noAUTO=1

#### TODO

- include ldap configuration in GLPI in Step6
- include mail configuration in GLPI in Step6
- include acpu php extension in Step1
- include file deletion of `install.php` in `GLPI/install/` in Step6
- include `CSS_DEV` personalisation (in case of DEV install) in `GLPI/css/palettes/` in Step6
